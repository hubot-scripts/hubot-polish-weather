'use strict';

// Description:
//   Allows to display information about weather in polish cities for today and 5 days in the future.
//   Supports fuzzy searching of cities and weekdays. Uses free API from the GeoNames and OpenWeather.
//
// Dependencies:
//   None
//
// Configuration:
//   HUBOT_WEATHER_OW_API_KEY - The API key for the OpenWeather API: https://openweathermap.org/api
//   HUBOT_WEATHER_GN_USER - The username for the GeoNames API: http://www.geonames.org/export/geonames-search.html
//
// Commands:
//   hubot pogoda w `MIASTO` na `DZIEŃ` o `GODZINA` - wyświetla pogodę dla wybranego miasta na konkretny dzień. Hubot zapamięta ostatnio wybrane miasto, domyślnie prezentuje prognozę pogody na "dzisiaj". {MIASTO - nazwa miasta, dla którego bot ma sprawdzić pogodę (bot spróbuje dopasować nazwę miasta przy pomocy logiki rozmytej).} {DATA - Opcjonalna data, dla której bot ma sprawdzić pogodę. Domyślnie ustawiona jako "dzisiaj". Nie może być większa niż 5 dni wprzód.} {GODZINA - Opcjonalna godzina, dla której bot ma sprawdzić pogodę. Domyślnie dobierze aktualną godzinę. Prognoza pogody jest wykonywana w 3-godzinnych interwałach. Może być podana w formacie `HH` lub `HH:mm`.} [POGODA] <hubot pogoda> <hubot pogoda w MIASTO> <hubot prognoza pogody dla MIASTO>
//
// Author:
//   Dorian Krefft <dorian.krefft@gmail.com>

const Responder = require('../src/responder');
const Helpers = require('../src/helpers');

module.exports = (robot) => {
    const responder = new Responder(robot, process.env.HUBOT_WEATHER_OW_API_KEY, process.env.HUBOT_WEATHER_GN_USER, 'pl');

    robot.respond(/(?:pogoda|prognoza\s+pogody)\s+(?:w|we|dla)\s+([\p{L} ]+)\s+(?:w|na|we)\s+(\p{L}+)(?:\s+o\s+(\d{2})(?::(\d{2}))?)?$/iu, (response) => {
        response.finish();
        const city = response.match[1];
        const date = response.match[2];
        const hours = response.match[3];
        const minutes = response.match[4];
        const day = Helpers.date.detectCorrectDate(date, hours, minutes);
        responder.displayWeatherSummary(city, day, response);
    });

    robot.respond(/(?:pogoda|prognoza\s+pogody)\s+(?:w|we|dla)\s+([\p{L} ]+)(?:\s+o\s+(\d{2})(?::(\d{2}))?)?$/iu, (response) => {
        response.finish();
        const city = response.match[1];
        const hours = response.match[2];
        const minutes = response.match[3];
        const day = Helpers.date.detectCorrectDate(null, hours, minutes);
        responder.displayWeatherSummary(city, day, response);
    });

    robot.respond(/(?:pogoda|prognoza\s+pogody)/i, (response) => {
        response.finish();
        const day = Helpers.date.detectCorrectDate(null);
        responder.displayWeatherSummary(null, day, response);
    });
};