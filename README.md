# hubot-polish-weather

Hubot script that allows to display information about weather in polish cities for today and 5 days in the future using only free API.

It's power lies in the fuzzy matching of cities and days - polish languages is very flexible and it's hard to predict all
the cases.

It uses the [OpenWeather API](https://openweathermap.org/api) to get weather forecasts for the specific city and the
[GeoNames API](http://www.geonames.org/export/geonames-search.html) to get correct name for the city (in the nominative case).

## Usage

### Installing the module

First step is to include the `hubot-polish-weather` dependency to your Hubot scripts (inside the package.json file). Then
all you have to do is to update your `external-scripts.json` file to include our library. If this file is missing, simply
create it with content:
```json
["hubot-polish-weather"]
```

### Set up the module
This module requires you to set up keys for the APIs.
1) For the OpenWeather API, please set up the `HUBOT_WEATHER_OW_API_KEY` environment variable
1) For the GeoNames API, please set up the `HUBOT_WEATHER_GN_USER` environment variable

### Gathered information
The bot will show information about all weathers forecast for the day on this specific hour.
Additionally, it will also display general data like temperature, humidity and pressure.

```text
Pogoda dla miasta Poznań jutro o 13:00:
Moja urocza pogodynka opisała pogodę w taki sposób: :partly_sunny:rozproszone chmury.
Garść statystyk: :thermometer:1.54 stopni Celsjusza, :cityscape:94 wilgotności, natomiast :stopwatch:ciśnienie wynosi 1002.82 hPa
```

### Examples of usage
I tried to make this library as flexible for the user as possible. 

If the user will pass only the name for the city, the library will assume, that he wants to see current weather forecast:
```text
hubot pogoda dla Gdańska
hubot pogoda w Krakowie
```
The bot will ignore diacritics and will display the weather forecast for the city with the biggest population in case
of more matching cities.

After first usage, the bot will remember last checked city for every user. It means that in this case he can just write:
```text
hubot pogoda w Warszawie
hubot pogoda
```
... and the bot will remember in the second case, that it should check the Warsaw city.

You can also tell the bot to check the weather forecast for maximum 5 days (since in the free version, the API only allows
so much days to be forecast). Because of this, I've decided to use *the weekday names* instead of dates (Monday, Tuesday,
tomorrow, today etc.):
```text
hubot pogoda w Warszawie na jutro // weather forecast for tomorrow
hubot pogoda w Warszawie w niedzielę // weather forecast for the Sunday
hubot pogoda w Warszawie we wtorek // weather forecast for the Tuesday
```
Of course, you can also pass the exact hour to be checked. Since the weather forecasts are done in the 3-hours intervals,
the bot will find the closest interval to the passed date:
```text
hubot pogoda w Warszawie o 14:30 // will show weather forecast for today, 15:00
hubot pogoda w Warszawie w poniedziałek o 18:00 // will show weather forecast for Monday, 18:00
hubot pogoda w Warszawie na jutro o 12:10 // will show weather forecast for tomorrow, 12:00
```

## What about other languages?
I tried to write this library in such a manner that will allow to easily port it to different language. 
I know that the polish version will not be popular, but there are many other libraries for 
[the english version](https://www.npmjs.com/search?q=hubot%20weather). Polish version of the library exists only because
of the requirement to use full cities and weekday names in the nominative case, which is fairly uncomfortable.
If you want help with implementing the fuzzy-logic weather forecast to your own language, feel free to ask me.

## Contribution
If you want to contribute, just fork the project and add some changes!

## Errors and ideas to improve
If you will find any errors with this library or have great idea how to improve it (and don't want to do it on your own),
please feel free to open [a new ticket](https://gitlab.com/hubot-scripts/hubot-polish-weather/issues). 
