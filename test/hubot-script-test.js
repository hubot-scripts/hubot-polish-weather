'use strict';

const HubotChatTesting = require('hubot-chat-testing');
const Helper = require('hubot-test-helper');
const Mocks = require('./helpers/mock-requests');
const moment = require('moment');

describe('Testing the bot responses', () => {
    const chat = new HubotChatTesting('hubot', new Helper('../scripts/index.js'));

    beforeEach(() => {
        Mocks.mockAllRequests();
    });

    context('Getting the weather summary for specific city without specifying the date', () => {
        chat.when('the user is asking about the weather summary of non-existing city')
            .user('alice').messagesBot('pogoda w niesitniejącym mieście')
            .bot.messagesRoom(':warning: Niestety, wygląda na to, że nie udało się odnaleźć miasta o nazwie \'niesitniejącym mieście\' :(')
            .expect('the bot should tell him that this city could not be found');

        chat.when('the user is asking about the weather summary of existing city')
            .user('alice').messagesBot('pogoda w Gdańsku')
            .bot.replyMatches(/Pogoda dla miasta Gdańsk dziś o \d{2}:\d{2}/)
            .and.itMatches(/Moja urocza pogodynka opisała pogodę w taki sposób: .+/)
            .and.itMatches(/Garść statystyk: :thermometer:.+ stopni Celsjusza, :cityscape:\d+ wilgotności, natomiast :stopwatch:ciśnienie wynosi [\d.]+ hPa/)
            .expect('the bot should show him generic weather information about today\'s date');
    });

    context('Remembering the previous city', () => {
        chat.when('the user is asking about the weather without specifying the city and he did not ask about it before')
            .user('alice').messagesBot('pogoda')
            .bot.repliesWith(':warning: Musisz podać nazwę miasta, abym mógł sprawdzić dla Ciebie pogodę - niestety, nie jestem jasnowidzem :)')
            .expect('the bot should tell the user that he does not know in which city he should check the weather');

        chat.when('the user is asking about the weather without specifying the city and he did ask about it before')
            .setBrain((brain) => {
                brain.set('HUBOT-WEATHER-LAST-CITY-alice', 'Gdańsk');
            })
            .user('alice').messagesBot('pogoda')
            .bot.replyMatches(/Pogoda dla miasta Gdańsk dziś o \d{2}:\d{2}/)
            .expect('the bot should tell the user that he does not know in which city he should check the weather');
    });

    context('Getting the weather for the specific date', () => {
        const farDay = moment().locale('pl').add(6, 'days').format('dddd');

        chat.when('the user is asking to forecast the weather for the day too far in the future')
            .user('alice').messagesBot(`pogoda w Gdańsku na ${farDay}`)
            .bot.messagesRoom(':warning: W darmowej wersji API nie można przewidywać pogody więcej niż 5 dni wprzód.')
            .expect('the bot should inform him that in the free version of the API key this cannot be done.');

        chat.when('the user is asking to forecast the weather for the day from the past')
            .user('alice').messagesBot(`pogoda w Gdańsku na wczoraj`)
            .bot.messagesRoom('Niestety, nie obsługuję wyświetlania pogody z dni poprzednich.')
            .expect('the bot should inform him that it does not support showing historical data.');
    });
});