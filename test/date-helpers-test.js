'use strict';

const expect = require('chai').expect;
const Helpers = require('../src/helpers').date;
const moment = require('moment');

describe('Testing the date helpers', () => {
    beforeEach(() => {
        this.dateFormat = 'DD/MM/YYYY';
        this.dateTimeFormat = 'DD/MM/YYYY HH:mm:SS';
        this.locale = 'pl';
    });

    context('Test fuzzy matching weekday', () => {
        it('should not find any weekday name that is not matching polish names', () => {
            const matchingWeekday = Helpers._findMatchingWeekday('not matching any of the weekdays', this.locale);
            expect(matchingWeekday).to.be.eq(null);
        });

        it('should find the weekday name that the exact name was passed', () => {
            const matchingWeekday = Helpers._findMatchingWeekday('niedziela', this.locale);
            expect(matchingWeekday).to.be.eq('niedziela');
        });

        it('should find the weekday name that was declined', () => {
            const matchingWeekday = Helpers._findMatchingWeekday('niedzielę', this.locale);
            expect(matchingWeekday).to.be.eq('niedziela');
        });

        it('should find the weekday name that has slight error', () => {
            const matchingWeekday = Helpers._findMatchingWeekday('neidiziela', this.locale);
            expect(matchingWeekday).to.be.eq('niedziela');
        });

        it('should ignore diacritics', () => {
            const matchingWeekday = Helpers._findMatchingWeekday('sroda', this.locale);
            expect(matchingWeekday).to.be.eq('środa');
        });
    });

    context('Test detecting the date of weekday', () => {
        it('should pick today if today\'s weekday name was passed', () => {
            const today = moment().locale(this.locale);
            const weekday = today.format('dddd');
            const result = Helpers._getNextWeekday(weekday, this.locale);
            expect(result.format(this.dateFormat)).to.be.eql(today.format(this.dateFormat));
        });

        it('should pick day from the next week if yesterday\'s weekday name was passed', () => {
            const yesterday = moment().subtract(1, 'days').locale(this.locale);
            const dayFromNextWeek = yesterday.add(1, 'week');
            const weekday = yesterday.format('dddd');
            const result = Helpers._getNextWeekday(weekday, this.locale);
            expect(result.format(this.dateFormat)).to.be.eq(dayFromNextWeek.format(this.dateFormat));
        });

        it('should pick day from current week if tomorrow\'s weekday name was passed', () => {
            const tomorrow = moment().add(1, 'days').locale(this.locale);
            const weekday = tomorrow.format('dddd');
            const result = Helpers._getNextWeekday(weekday, this.locale);
            expect(result.format(this.dateFormat)).to.be.eq(tomorrow.format(this.dateFormat));
        });
    });

    context('Test matching user-friendly day names', () => {
        it('should just detect today\'s date when no aliases were passed', () => {
            const today = moment();
            const date = Helpers.detectCorrectDate(null);
            expect(date.format(this.dateFormat)).to.be.eq(today.format(this.dateFormat));
        });

        it('should detect correct date when today\'s aliases were passed', () => {
            const today = moment();
            const todayAliases = ['dzisiaj', 'dzis', 'dziś'];
            for(const alias of todayAliases){
                const date = Helpers.detectCorrectDate(alias);
                expect(date.format(this.dateFormat)).to.be.eq(today.format(this.dateFormat),
                    `The '${alias}' alias for today is not correctly processed`);
            }
        });

        it('should detect correct date when tomorrow\'s alias was passed', () => {
            const tomorrow = moment().add(1, 'days');
            const date = Helpers.detectCorrectDate('jutro');
            expect(date.format(this.dateFormat)).to.be.eq(tomorrow.format(this.dateFormat));
        });

        it('should detect correct date when the "two days after today" alias was passed', () => {
            const twoDaysAfterToday = moment().add(2, 'days');
            const date = Helpers.detectCorrectDate('pojutrze');
            expect(date.format(this.dateFormat)).to.be.eq(twoDaysAfterToday.format(this.dateFormat));
        });

        it('should detect correct date when the weekday name was passed', () => {
            const day = moment().add(3, 'days');
            const weekday = day.format('dddd');
            const date = Helpers.detectCorrectDate(weekday);
            expect(date.format(this.dateFormat)).to.be.eq(day.format(this.dateFormat));
        });

        it('should return date with correct time when it was provided', () => {
            const hours = 19;
            const minutes = 30;
            const day = moment().add(3, 'days').hours(hours).minutes(minutes).seconds(0).milliseconds(0);
            const weekday = day.format('dddd');
            const date = Helpers.detectCorrectDate(weekday, hours, minutes);
            expect(date.format(this.dateTimeFormat)).to.be.eq(day.format(this.dateTimeFormat));
        });
    });

    context('Test calculating days difference from today', () => {
        it('should return 0 when today is passed', () => {
           const today = moment();
           const result = Helpers.calculateDaysFromToday(today);
           expect(result).to.be.eq(0);
        });

        it('should return days difference when the future date is passed', () => {
            const daysDifference = 4;
            const day = moment().add(daysDifference, 'days');
            const result = Helpers.calculateDaysFromToday(day);
            expect(result).to.be.eq(daysDifference);
        });

        it('should return days difference when the past date is passed', () => {
            const daysDifference = 4;
            const day = moment().subtract(daysDifference, 'days');
            const result = Helpers.calculateDaysFromToday(day);
            expect(result).to.be.eq(-daysDifference);
        });
    });
});