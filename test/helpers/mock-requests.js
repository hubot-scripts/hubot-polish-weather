'use strict';

const nock = require('nock');
const geoNamesData = require('./data/geonames-mock');
const openWeatherData = require('./data/openweather-mock');

class MockRequests {
    static mockAllRequests() {
        nock.disableNetConnect();
        this.mockGeoNamesApi();
        this.mockOpenWeatherApi();
    }

    static mockOpenWeatherApi() {
        nock('http://api.openweathermap.org:80')
            .persist()
            .get('/data/2.5/forecast')
            .query(true)
            .reply((uri, requestBody, cb) => {
                const city = MockRequests._getParamFromUri(uri, 'q');
                if(city.startsWith('gda')){
                    cb(null, [200, openWeatherData]);
                }
                else{
                    cb(null, [200, {'cod': '404'}]);
                }
            });
    }

    static mockGeoNamesApi() {
        nock('http://api.geonames.org')
            .get('/searchJSON')
            .query(true)
            .reply(200, (uri) => {
                const city = MockRequests._getParamFromUri(uri, 'q');
                if (city.startsWith('gda')) {
                    return [200, geoNamesData];
                }
                else {
                    return [200, {
                        "cod": "404",
                        "message": "city not found"
                    }];
                }
            });
    }

    static _getParamFromUri(uri, param) {
        const url = new URL(`http://test${uri}`);
        url.searchParams.get(param).toLowerCase();
        return url.searchParams.get(param).toLowerCase();
    }
}

module.exports = MockRequests;