'use strict';

const moment = require('moment');

const data = {
    'cod': '200',
    'message': 0.0044,
    'cnt': 40,
    'list': [{
        'dt': 1548957600,
        'main': {
            'temp': -0.85,
            'temp_min': -0.85,
            'temp_max': 1.07,
            'pressure': 1013.36,
            'sea_level': 1018.36,
            'grnd_level': 1013.36,
            'humidity': 100,
            'temp_kf': -1.92
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'słabe opady śniegu', 'icon': '13n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 5.56, 'deg': 135.503},
        'snow': {'3h': 0.1745},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-01-31 18:00:00'
    }, {
        'dt': 1548968400,
        'main': {
            'temp': -0.68,
            'temp_min': -0.68,
            'temp_max': 0.76,
            'pressure': 1013.14,
            'sea_level': 1018.17,
            'grnd_level': 1013.14,
            'humidity': 100,
            'temp_kf': -1.44
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'słabe opady śniegu', 'icon': '13n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 4.72, 'deg': 146.502},
        'snow': {'3h': 0.039},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-01-31 21:00:00'
    }, {
        'dt': 1548979200,
        'main': {
            'temp': -0.78,
            'temp_min': -0.78,
            'temp_max': 0.18,
            'pressure': 1012.69,
            'sea_level': 1017.65,
            'grnd_level': 1012.69,
            'humidity': 100,
            'temp_kf': -0.96
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'słabe opady śniegu', 'icon': '13n'}],
        'clouds': {'all': 68},
        'wind': {'speed': 3.31, 'deg': 169.003},
        'snow': {'3h': 0.044},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-01 00:00:00'
    }, {
        'dt': 1548990000,
        'main': {
            'temp': -1.48,
            'temp_min': -1.48,
            'temp_max': -1.01,
            'pressure': 1012.34,
            'sea_level': 1017.3,
            'grnd_level': 1012.34,
            'humidity': 100,
            'temp_kf': -0.48
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 44},
        'wind': {'speed': 3.56, 'deg': 181.002},
        'snow': {'3h': 0.022},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-01 03:00:00'
    }, {
        'dt': 1549000800,
        'main': {
            'temp': -1.38,
            'temp_min': -1.38,
            'temp_max': -1.38,
            'pressure': 1012.22,
            'sea_level': 1017.2,
            'grnd_level': 1012.22,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 64},
        'wind': {'speed': 4.92, 'deg': 165.509},
        'snow': {'3h': 0.0275},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-01 06:00:00'
    }, {
        'dt': 1549011600,
        'main': {
            'temp': -1.07,
            'temp_min': -1.07,
            'temp_max': -1.07,
            'pressure': 1012.45,
            'sea_level': 1017.32,
            'grnd_level': 1012.45,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01d'}],
        'clouds': {'all': 20},
        'wind': {'speed': 6.92, 'deg': 160.507},
        'snow': {'3h': 0.02625},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-01 09:00:00'
    }, {
        'dt': 1549022400,
        'main': {
            'temp': 0.64,
            'temp_min': 0.64,
            'temp_max': 0.64,
            'pressure': 1011.17,
            'sea_level': 1016.13,
            'grnd_level': 1011.17,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01d'}],
        'clouds': {'all': 48},
        'wind': {'speed': 7.26, 'deg': 143.504},
        'snow': {'3h': 0.00125},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-01 12:00:00'
    }, {
        'dt': 1549033200,
        'main': {
            'temp': 1.01,
            'temp_min': 1.01,
            'temp_max': 1.01,
            'pressure': 1010.67,
            'sea_level': 1015.64,
            'grnd_level': 1010.67,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 801, 'main': 'Clouds', 'description': 'lekkie zachmurzenie', 'icon': '02d'}],
        'clouds': {'all': 12},
        'wind': {'speed': 8.71, 'deg': 140.004},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-01 15:00:00'
    }, {
        'dt': 1549044000,
        'main': {
            'temp': 0.68,
            'temp_min': 0.68,
            'temp_max': 0.68,
            'pressure': 1010.35,
            'sea_level': 1015.29,
            'grnd_level': 1010.35,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 803, 'main': 'Clouds', 'description': 'pochmurno z przejaśnieniami', 'icon': '04n'}],
        'clouds': {'all': 64},
        'wind': {'speed': 9.76, 'deg': 145.001},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-01 18:00:00'
    }, {
        'dt': 1549054800,
        'main': {
            'temp': -0.07,
            'temp_min': -0.07,
            'temp_max': -0.07,
            'pressure': 1010.49,
            'sea_level': 1015.44,
            'grnd_level': 1010.49,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 802, 'main': 'Clouds', 'description': 'rozproszone chmury', 'icon': '03n'}],
        'clouds': {'all': 48},
        'wind': {'speed': 7.87, 'deg': 150.009},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-01 21:00:00'
    }, {
        'dt': 1549065600,
        'main': {
            'temp': 0.74,
            'temp_min': 0.74,
            'temp_max': 0.74,
            'pressure': 1009.71,
            'sea_level': 1014.73,
            'grnd_level': 1009.71,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 6.51, 'deg': 140.501},
        'snow': {'3h': 0.0049999999999999},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-02 00:00:00'
    }, {
        'dt': 1549076400,
        'main': {
            'temp': 1.38,
            'temp_min': 1.38,
            'temp_max': 1.38,
            'pressure': 1008.85,
            'sea_level': 1013.74,
            'grnd_level': 1008.85,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 76},
        'wind': {'speed': 6.92, 'deg': 134.003},
        'rain': {'3h': 0.025},
        'snow': {'3h': 0.0025000000000002},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-02 03:00:00'
    }, {
        'dt': 1549087200,
        'main': {
            'temp': 2.07,
            'temp_min': 2.07,
            'temp_max': 2.07,
            'pressure': 1007.89,
            'sea_level': 1012.78,
            'grnd_level': 1007.89,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 7.06, 'deg': 120.003},
        'rain': {'3h': 0.18},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-02 06:00:00'
    }, {
        'dt': 1549098000,
        'main': {
            'temp': 3.26,
            'temp_min': 3.26,
            'temp_max': 3.26,
            'pressure': 1006.99,
            'sea_level': 1011.9,
            'grnd_level': 1006.99,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10d'}],
        'clouds': {'all': 92},
        'wind': {'speed': 7.12, 'deg': 112.003},
        'rain': {'3h': 0.245},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-02 09:00:00'
    }, {
        'dt': 1549108800,
        'main': {
            'temp': 4.33,
            'temp_min': 4.33,
            'temp_max': 4.33,
            'pressure': 1005.7,
            'sea_level': 1010.61,
            'grnd_level': 1005.7,
            'humidity': 96,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10d'}],
        'clouds': {'all': 92},
        'wind': {'speed': 5.76, 'deg': 121.005},
        'rain': {'3h': 0.32},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-02 12:00:00'
    }, {
        'dt': 1549119600,
        'main': {
            'temp': 4.63,
            'temp_min': 4.63,
            'temp_max': 4.63,
            'pressure': 1004.79,
            'sea_level': 1009.56,
            'grnd_level': 1004.79,
            'humidity': 94,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10d'}],
        'clouds': {'all': 92},
        'wind': {'speed': 5.66, 'deg': 137.001},
        'rain': {'3h': 0.25},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-02 15:00:00'
    }, {
        'dt': 1549130400,
        'main': {
            'temp': 4.8,
            'temp_min': 4.8,
            'temp_max': 4.8,
            'pressure': 1004.34,
            'sea_level': 1009.24,
            'grnd_level': 1004.34,
            'humidity': 93,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 6.82, 'deg': 150.005},
        'rain': {'3h': 0.26},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-02 18:00:00'
    }, {
        'dt': 1549141200,
        'main': {
            'temp': 4.97,
            'temp_min': 4.97,
            'temp_max': 4.97,
            'pressure': 1003.97,
            'sea_level': 1008.77,
            'grnd_level': 1003.97,
            'humidity': 92,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 76},
        'wind': {'speed': 7.34, 'deg': 161.005},
        'rain': {'3h': 0.05},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-02 21:00:00'
    }, {
        'dt': 1549152000,
        'main': {
            'temp': 4.83,
            'temp_min': 4.83,
            'temp_max': 4.83,
            'pressure': 1004.22,
            'sea_level': 1009.18,
            'grnd_level': 1004.22,
            'humidity': 93,
            'temp_kf': 0
        },
        'weather': [{'id': 802, 'main': 'Clouds', 'description': 'rozproszone chmury', 'icon': '03n'}],
        'clouds': {'all': 32},
        'wind': {'speed': 7.07, 'deg': 173.5},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-03 00:00:00'
    }, {
        'dt': 1549162800,
        'main': {
            'temp': 4.08,
            'temp_min': 4.08,
            'temp_max': 4.08,
            'pressure': 1004.93,
            'sea_level': 1009.87,
            'grnd_level': 1004.93,
            'humidity': 96,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 6.67, 'deg': 187.506},
        'rain': {'3h': 0.63},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-03 03:00:00'
    }, {
        'dt': 1549173600,
        'main': {
            'temp': 2.48,
            'temp_min': 2.48,
            'temp_max': 2.48,
            'pressure': 1006.53,
            'sea_level': 1011.46,
            'grnd_level': 1006.53,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 20},
        'wind': {'speed': 6.87, 'deg': 205.503},
        'rain': {'3h': 0.55},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-03 06:00:00'
    }, {
        'dt': 1549184400,
        'main': {
            'temp': 1.48,
            'temp_min': 1.48,
            'temp_max': 1.48,
            'pressure': 1008.23,
            'sea_level': 1013.28,
            'grnd_level': 1008.23,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10d'}],
        'clouds': {'all': 20},
        'wind': {'speed': 7.7, 'deg': 213},
        'rain': {'3h': 0.01},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-03 09:00:00'
    }, {
        'dt': 1549195200,
        'main': {
            'temp': 3.04,
            'temp_min': 3.04,
            'temp_max': 3.04,
            'pressure': 1010.11,
            'sea_level': 1015.07,
            'grnd_level': 1010.11,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10d'}],
        'clouds': {'all': 48},
        'wind': {'speed': 8.01, 'deg': 233.501},
        'rain': {'3h': 0.04},
        'snow': {'3h': 0.0024999999999999},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-03 12:00:00'
    }, {
        'dt': 1549206000,
        'main': {
            'temp': 2.77,
            'temp_min': 2.77,
            'temp_max': 2.77,
            'pressure': 1012.82,
            'sea_level': 1017.76,
            'grnd_level': 1012.82,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01d'}],
        'clouds': {'all': 80},
        'wind': {'speed': 8.89, 'deg': 255.001},
        'rain': {},
        'snow': {'3h': 0.0049999999999999},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-03 15:00:00'
    }, {
        'dt': 1549216800,
        'main': {
            'temp': 2.24,
            'temp_min': 2.24,
            'temp_max': 2.24,
            'pressure': 1015.4,
            'sea_level': 1020.43,
            'grnd_level': 1015.4,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 56},
        'wind': {'speed': 9.07, 'deg': 264.002},
        'rain': {'3h': 0.0099999999999998},
        'snow': {'3h': 0.0075000000000001},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-03 18:00:00'
    }, {
        'dt': 1549227600,
        'main': {
            'temp': 2.15,
            'temp_min': 2.15,
            'temp_max': 2.15,
            'pressure': 1017.88,
            'sea_level': 1022.93,
            'grnd_level': 1017.88,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'słabe opady deszczu', 'icon': '10n'}],
        'clouds': {'all': 68},
        'wind': {'speed': 8.67, 'deg': 267.501},
        'rain': {'3h': 0.01},
        'snow': {'3h': 0.0075000000000001},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-03 21:00:00'
    }, {
        'dt': 1549238400,
        'main': {
            'temp': 1.78,
            'temp_min': 1.78,
            'temp_max': 1.78,
            'pressure': 1020.38,
            'sea_level': 1025.33,
            'grnd_level': 1020.38,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 6.92, 'deg': 269.501},
        'rain': {},
        'snow': {'3h': 0.0024999999999999},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-04 00:00:00'
    }, {
        'dt': 1549249200,
        'main': {
            'temp': 1.37,
            'temp_min': 1.37,
            'temp_max': 1.37,
            'pressure': 1021.8,
            'sea_level': 1026.82,
            'grnd_level': 1021.8,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 7.17, 'deg': 270.502},
        'rain': {},
        'snow': {'3h': 0.0175},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-04 03:00:00'
    }, {
        'dt': 1549260000,
        'main': {
            'temp': 0.94,
            'temp_min': 0.94,
            'temp_max': 0.94,
            'pressure': 1023.7,
            'sea_level': 1028.65,
            'grnd_level': 1023.7,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 7.12, 'deg': 266},
        'rain': {},
        'snow': {'3h': 0.0049999999999999},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-04 06:00:00'
    }, {
        'dt': 1549270800,
        'main': {
            'temp': 0.9,
            'temp_min': 0.9,
            'temp_max': 0.9,
            'pressure': 1025.37,
            'sea_level': 1030.44,
            'grnd_level': 1025.37,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01d'}],
        'clouds': {'all': 24},
        'wind': {'speed': 6.61, 'deg': 258.506},
        'rain': {},
        'snow': {'3h': 0.0050000000000001},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-04 09:00:00'
    }, {
        'dt': 1549281600,
        'main': {
            'temp': 1.17,
            'temp_min': 1.17,
            'temp_max': 1.17,
            'pressure': 1026.83,
            'sea_level': 1031.9,
            'grnd_level': 1026.83,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01d'}],
        'clouds': {'all': 56},
        'wind': {'speed': 7.21, 'deg': 255.501},
        'rain': {},
        'snow': {'3h': 0.025},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-04 12:00:00'
    }, {
        'dt': 1549292400,
        'main': {
            'temp': 0.21,
            'temp_min': 0.21,
            'temp_max': 0.21,
            'pressure': 1028.38,
            'sea_level': 1033.38,
            'grnd_level': 1028.38,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'słabe opady śniegu', 'icon': '13d'}],
        'clouds': {'all': 56},
        'wind': {'speed': 9.62, 'deg': 266.507},
        'rain': {},
        'snow': {'3h': 0.035},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-04 15:00:00'
    }, {
        'dt': 1549303200,
        'main': {
            'temp': 0.32,
            'temp_min': 0.32,
            'temp_max': 0.32,
            'pressure': 1030.04,
            'sea_level': 1035.14,
            'grnd_level': 1030.04,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'słabe opady śniegu', 'icon': '13n'}],
        'clouds': {'all': 76},
        'wind': {'speed': 10.47, 'deg': 280.504},
        'rain': {},
        'snow': {'3h': 0.0475},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-04 18:00:00'
    }, {
        'dt': 1549314000,
        'main': {
            'temp': 1.19,
            'temp_min': 1.19,
            'temp_max': 1.19,
            'pressure': 1031.89,
            'sea_level': 1037,
            'grnd_level': 1031.89,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'słabe opady śniegu', 'icon': '13n'}],
        'clouds': {'all': 56},
        'wind': {'speed': 10.61, 'deg': 291},
        'rain': {},
        'snow': {'3h': 0.0425},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-04 21:00:00'
    }, {
        'dt': 1549324800,
        'main': {
            'temp': 1.23,
            'temp_min': 1.23,
            'temp_max': 1.23,
            'pressure': 1034.31,
            'sea_level': 1039.38,
            'grnd_level': 1034.31,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 0},
        'wind': {'speed': 7.88, 'deg': 295.504},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-05 00:00:00'
    }, {
        'dt': 1549335600,
        'main': {
            'temp': -0.4,
            'temp_min': -0.4,
            'temp_max': -0.4,
            'pressure': 1035.61,
            'sea_level': 1040.68,
            'grnd_level': 1035.61,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 0},
        'wind': {'speed': 5.81, 'deg': 252},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-05 03:00:00'
    }, {
        'dt': 1549346400,
        'main': {
            'temp': -0.56,
            'temp_min': -0.56,
            'temp_max': -0.56,
            'pressure': 1036.27,
            'sea_level': 1041.46,
            'grnd_level': 1036.27,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01n'}],
        'clouds': {'all': 0},
        'wind': {'speed': 6.01, 'deg': 238.002},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2019-02-05 06:00:00'
    }, {
        'dt': 1549357200,
        'main': {
            'temp': -0.47,
            'temp_min': -0.47,
            'temp_max': -0.47,
            'pressure': 1037.07,
            'sea_level': 1042.21,
            'grnd_level': 1037.07,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01d'}],
        'clouds': {'all': 0},
        'wind': {'speed': 6.82, 'deg': 226},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-05 09:00:00'
    }, {
        'dt': 1549368000,
        'main': {
            'temp': 1.31,
            'temp_min': 1.31,
            'temp_max': 1.31,
            'pressure': 1037.28,
            'sea_level': 1042.32,
            'grnd_level': 1037.28,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'bezchmurnie', 'icon': '01d'}],
        'clouds': {'all': 0},
        'wind': {'speed': 6.47, 'deg': 222.003},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-05 12:00:00'
    }, {
        'dt': 1549378800,
        'main': {
            'temp': 1.28,
            'temp_min': 1.28,
            'temp_max': 1.28,
            'pressure': 1036.93,
            'sea_level': 1042,
            'grnd_level': 1036.93,
            'humidity': 100,
            'temp_kf': 0
        },
        'weather': [{'id': 802, 'main': 'Clouds', 'description': 'rozproszone chmury', 'icon': '03d'}],
        'clouds': {'all': 36},
        'wind': {'speed': 6.62, 'deg': 214.012},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2019-02-05 15:00:00'
    }],
    'city': {'id': 7531002, 'name': 'Gdańsk', 'coord': {'lat': 54.3476, 'lon': 18.6452}, 'country': 'PL'}
};

function updateTimestamps(date) {
    const firstDate = moment.unix(data.list[0].dt);
    for (const weather of data.list) {
        const d = moment.unix(weather.dt);
        const diff = d.diff(firstDate, 'minutes');
        const updated = date.add(diff, 'minutes').hours(d.hours()).minutes(d.minutes()).seconds(d.seconds());
        weather.dt = updated.unix();
        weather.dt_txt = updated.format('YYYY-MM-DD HH:mm:SS');
    }

    return data;
}

module.exports = updateTimestamps(moment());