'use strict';

module.exports = {
    'totalResultsCount': 1,
    'geonames': [{
        'adminCode1': '82',
        'lng': '18.64637',
        'geonameId': 3099434,
        'toponymName': 'Gdańsk',
        'countryId': '798544',
        'fcl': 'P',
        'population': 461865,
        'countryCode': 'PL',
        'name': 'Gdańsk',
        'fclName': 'city, village,...',
        'adminCodes1': {'ISO3166_2': 'PM'},
        'countryName': 'Rzeczpospolita Polska',
        'fcodeName': 'seat of a first-order administrative division',
        'adminName1': 'Województwo pomorskie',
        'lat': '54.35205',
        'fcode': 'PPLA'
    }]
};