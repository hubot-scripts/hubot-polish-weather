'use strict';

const expect = require('chai').expect;
const WeatherEmoticons = require('../src/weather-emoticons');

describe('Testing getting the emoticons for specific weather status', () => {
    context('Calculating the general emoticon code', () => {
        it('should return main code for any weather code', () => {
            const code = '388';
            const mainCode = WeatherEmoticons._getGeneralEmotCode(code);
            expect(mainCode).to.be.eq('3xx');
        });
    });

    context('Getting the emoticon for specific weather status', () => {
        it('should return specific emoticon when it is defined for specific status', () => {
            const code = '804';
            const emot = WeatherEmoticons.getEmoticonByCode(code);
            expect(emot).to.be.eq(':cloud:');
        });

        it('should return emoticon for the weather group when it there is no emoticon defined for specific status', () => {
            const code = '899';
            const emot = WeatherEmoticons.getEmoticonByCode(code);
            expect(emot).to.be.eq(':sunny:');
        });

        it('should return null when it does not find emoticon for specific status nor for group', () => {
            const code = 'not existing in the list';
            const emot = WeatherEmoticons.getEmoticonByCode(code);
            expect(emot).to.be.eql(undefined);
        });
    });
});