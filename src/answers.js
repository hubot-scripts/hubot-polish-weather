const WeatherEmoticons = require('./weather-emoticons');

const emoticons = {
    'temperature': ':thermometer:',
    'humidity': ':cityscape:',
    'pressure': ':stopwatch:',
    'sunrise': ':sunrise:',
    'sunset': ':city_sunset:',
    'warning' : ':warning:'
};

class WeatherAnswers {
    static generateSummary(city, data) {
        const day = data.date.locale('pl').calendar().toLowerCase();
        const answer = [`Pogoda dla miasta ${city} ${day}:`];
        answer.push(this._getWeathersInfo(data.weather));
        answer.push(this._getMainInfo(data));
        return answer.join('\n');
    }

    static _getWeathersInfo(weathers) {
        const answers = [];
        for (const weather of weathers) {
            answers.push(this._getWeatherByCode(weather));
        }
        const prettyAnswer = this._prettyPrintArray(answers, ' oraz ');
        return `Moja urocza pogodynka opisała pogodę w taki sposób: ${prettyAnswer}.`;
    }

    static _getMainInfo(info) {
        const infos = [
            `${emoticons.temperature}${info.temp} stopni Celsjusza`,
            `${emoticons.humidity}${info.humidity} wilgotności`,
            `${emoticons.pressure}ciśnienie wynosi ${info.pressure} hPa`
        ];
        const prettyAnswer = this._prettyPrintArray(infos, ', natomiast ');
        return `Garść statystyk: ${prettyAnswer}`;
    }

    static _getWeatherByCode(weather) {
        const emoticon = WeatherEmoticons.getEmoticonByCode(weather.id);
        return `${emoticon}${weather.description}`;
    }

    static _prettyPrintArray(array, joining) {
        if (array.length > 1) {
            const lastElement = array.pop();
            const firstPart = array.join(', ');
            return `${firstPart}${joining}${lastElement}`;
        }
        else {
            return array.join(',');
        }
    }
}

class ErrorAnswers {
    static cityNotFound(city) {
        return `${emoticons.warning} Niestety, wygląda na to, że nie udało się odnaleźć miasta o nazwie '${city}' :(`;
    }

    static invalidOWApiKey() {
        return `${emoticons.warning} Wygląda na to, że podany klucz API jest nieprawidłowy! Sprawdź proszę, czy zmienna HUBOT_WEATHER_OW_API_KEY jest prawidłowo ustawiona.`;
    }

    static generalOWError(error) {
        return `${emoticons.warning} Usługa do pobierania pogody zwróciła błąd w statusie \`${error.cod}\`: \`${error.message}\``;
    }

    static invalidGeonamesUser() {
        return `${emoticons.warning} Wygląda na to, że użytkownik dla API Geonames.org jest nieprawidłowy! Sprawdź proszę, czy zmienna HUBOT_WEATHER_GN_USER jest prawidłowo ustawiona`;
    }

    static cityNotProvided() {
        return `${emoticons.warning} Musisz podać nazwę miasta, abym mógł sprawdzić dla Ciebie pogodę - niestety, nie jestem jasnowidzem :)`;
    }

    static tooFarInFuture(){
        return `:warning: W darmowej wersji API nie można przewidywać pogody więcej niż 5 dni wprzód.`;
    }

    static dateInThePast(){
        return `Niestety, nie obsługuję wyświetlania pogody z dni poprzednich.`;
    }
}

const Answers = {
    errors: ErrorAnswers,
    weather: WeatherAnswers
};

module.exports = Answers;