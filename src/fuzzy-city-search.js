// TODO: Maybe extract this to another NPM module?
class FuzzyCitySearch {
    constructor(robot, username, fuzzy){
        this.robot = robot;
        this.username = username;
        this.fuzzy = fuzzy;
    }

    searchFuzzy(city, country){
        return this.robot.http(this._generateUrl(city, country)).get();
    }

    // Private methods
    _generateUrl(city, country){
        const parts = [
            'http://api.geonames.org/searchJSON?maxRows=1&featureClass=P&orderby=population&lang=pl',
            encodeURI(`q=${city}`), `username=${this.username}`, `fuzzy=${this.fuzzy}`, `country=${country}`

        ];
        return parts.join('&');
    }
}

module.exports = FuzzyCitySearch;