const Weather = require('./weather');
const FuzzyCitySearch = require('./fuzzy-city-search');
const Helpers = require('./helpers');
const Answers = require('./answers');

class Responder {
    constructor(robot, openWeatherApiKey, geoNamesUser, country) {
        this.country = country;
        this.robot = robot;
        this.weather = new Weather(robot, openWeatherApiKey, country, 'metric');
        this.citySearch = new FuzzyCitySearch(robot, geoNamesUser, 0.6);
        this.cityNameBrainKey = 'HUBOT-WEATHER-LAST-CITY';
    }

    displayWeatherSummary(city, date, response) {
        this._checkCityName(city, response, (name) => {
            this.weather.getWeatherInfo(name, date, (data) => {
                this.robot.logger.info(`Data received from the Weather API: ${JSON.stringify(data, undefined, 4)}`);
                response.reply(Answers.weather.generateSummary(name, data));
            }, Helpers.general.handleApiError(this.robot, response, name));
        });
    }

    _checkCityName(city, response, callback) {
        const username = response.message.user.name;
        const brainKey = `${this.cityNameBrainKey}-${username}`;

        if (!city) {
            const name = this.robot.brain.get(brainKey);
            if (!name) {
                response.reply(Answers.errors.cityNotProvided());
            }
            else {
                this.robot.brain.set(brainKey, name);
                callback(name);
            }
        }
        else {
            this.citySearch.searchFuzzy(city, this.country)((err, res, body) => {
                this.robot.logger.info(`Response from the geonames for ${city}:\n${body}`);
                const name = Helpers.general.getCityNameFromGeonames(this.robot, body, response);
                const usedName = name || city;
                this.robot.logger.info(`The Geonames.org returned name: ${name} for city ${city}. Using ${usedName}...`);
                this.robot.brain.set(brainKey, usedName);
                callback(usedName);
            });
        }
    }
}

module.exports = Responder;