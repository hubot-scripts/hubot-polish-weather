// https://openweathermap.org/weather-conditions
const openWeatherEmoticons = {
    '2xx': ':cloud_lightning:',
    '3xx': ':cloud_rain:',
    '5xx': ':cloud_rain:',
    '6xx': ':cloud_snow:',
    '7xx': ':fog:',
    '8xx': ':sunny:',
    '800': ':sunny:',
    '801': ':white_sun_small_cloud:',
    '802': ':partly_sunny:',
    '803': ':white_sun_cloud:',
    '804': ':cloud:',
};

class WeatherEmoticons {
    static getEmoticonByCode(code) {
        let emot = openWeatherEmoticons[code];
        if(!emot){
            const generalCode = this._getGeneralEmotCode(code);
            emot = openWeatherEmoticons[generalCode];
        }
        return emot;
    }

    static _getGeneralEmotCode(code) {
        const n = parseInt(code);
        const main = Math.floor(n / 100);
        return `${main}xx`;
    }
}

module.exports = WeatherEmoticons;