const moment = require('moment');
const weather = require('openweather-apis');
const helpers = require('./helpers');

class Weather {
    constructor(robot, openWeatherApiKey, language, system) {
        this.weather = Weather._initWeather(openWeatherApiKey, language, system);
        this.robot = robot;
    }

    getWeatherInfo(city, day, func, errorFunc) {
        this.weather.setCity(city);
        const difference = helpers.date.calculateDaysFromToday(day);

        this.robot.logger.info(`Getting the weather forecast for the ${day} - ${difference} days from today...`);

        if(difference > 5){
            errorFunc({
                cod: 999,
                message: "In the free version of API key you cannot forecast for more than 5 days."
            });
        }
        else if(difference < 0){
            errorFunc({
                cod: 998,
                message: "You cannot show the forecast for the past."
            });
        }
        else {
            this._getForecastForDay(day, func, errorFunc);
        }
    }

    _getForecastForDay(day, f, errorFunc) {
        this.weather.getWeatherForecast((err, data) => {
            this.robot.logger.info(`TEST: ${JSON.stringify(data)}`);
            if (parseInt(data.cod) !== 200) {
                errorFunc(data);
            }
            else{
                const forecast = Weather._findForecastForDay(day, data.list);
                const weather = forecast ? {
                    temp: forecast.main.temp,
                    pressure: forecast.main.pressure,
                    humidity: forecast.main.humidity,
                    weather: forecast.weather,
                    date: moment.unix(forecast.dt)
                } : null;
                f(weather);
            }
        });
    }

    static _findForecastForDay(day, forecasts){
        let min = null;
        let closest;
        for(const forecast of forecasts){
            const diff = Math.abs(moment.unix(forecast.dt).diff(day, 'seconds'));
            if(min && min < diff){
                break;
            }
            closest = forecast;
            min = diff;
        }
        return closest;
    }

    static _initWeather(apiKey, language, system) {
        weather.setLang(language);
        weather.setUnits(system);
        weather.setAPPID(apiKey);
        return weather;
    }
}

module.exports = Weather;