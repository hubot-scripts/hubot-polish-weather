const Fuse = require('fuse.js');
const moment = require('moment');
const Answers = require('./answers');

class GeneralHelpers {
    static handleApiError(robot, response, city) {
        return (data) => {
            robot.logger.error(`Error during collecting data from the weather API: ${data.cod}:\n${JSON.stringify(data, undefined, 3)}`);

            switch (parseInt(data.cod)) {
                case 404:
                    response.send(Answers.errors.cityNotFound(city));
                    break;
                case 401:
                    response.send(Answers.errors.invalidOWApiKey());
                    break;
                case 999:
                    response.send(Answers.errors.tooFarInFuture());
                    break;
                case 998:
                    response.send(Answers.errors.dateInThePast());
                    break;
                default:
                    response.send(Answers.errors.generalOWError(data));
            }
        };
    }

    static getCityNameFromGeonames(robot, body, response) {
        const data = JSON.parse(body);
        let name = null;
        if (data.geonames && data.geonames.length > 0) {
            name = data.geonames[0].toponymName;
        }
        else if (data.status) {
            robot.logger.error(`Error during collecting response from the Geonames API: ${JSON.stringify(data, undefined, 3)}`);

            switch (data.status.message) {
                case 'user does not exist.':
                    response.send(Answers.errors.invalidGeonamesUser());
                    break;
            }
        }
        return name;
    }
}

class DateHelpers {
    // TODO: Maybe make this language-proof?
    static detectCorrectDate(dateStr, hours, minutes) {
        const locale = 'pl';
        let date = moment().locale(locale);
        switch (dateStr) {
            case null:
            case 'dziś':
            case 'dzisiaj':
            case 'dzis':
                date = moment();
                break;
            case 'jutro':
                date = moment().add(1, 'day');
                break;
            case 'pojutrze':
                date = moment().add(2, 'days');
                break;
            case 'wczoraj':
                date = moment().subtract(1, 'day');
                break;
            default:
                const weekday = this._findMatchingWeekday(dateStr, locale);
                if (weekday) {
                    date = this._getNextWeekday(weekday, locale);
                }
        }
        if (hours) {
            date.hours(parseInt(hours));
        }
        if (minutes) {
            date.minutes(parseInt(minutes));
        }
        date.seconds(0).milliseconds(0);
        return date;
    }

    static _getNextWeekday(day, locale) {
        const d = moment();
        if (locale) {
            d.locale(locale);
        }
        d.day(day);
        const today = moment().hour(0).minute(0).second(0);
        if (d.isBefore(today)) {
            d.add(1, 'week');
        }
        return d;
    }

    static _findMatchingWeekday(day, locale) {
        if (locale) {
            moment.locale(locale);
        }
        const weekdays = moment.weekdays();
        const fuse = new Fuse(weekdays, {
            caseSensitive: true,
            shouldSort: true,
            threshold: 0.6,
            location: 0,
            distance: 100,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            keys: undefined
        });
        const result = fuse.search(day);
        if (result.length > 0) {
            return weekdays[result[0]];
        }
        return null;
    }

    static calculateDaysFromToday(day) {
        let difference = 0;
        const today = moment().hours(0).minutes(0).seconds(0);

        if(day.isBefore(moment())){
            difference = moment(day).hours(0).minutes(0).seconds(0).diff(today, 'days');
        }
        else if (day) {
            difference = moment(day).hours(0).minutes(0).seconds(1).diff(today, 'days');
        }
        return difference;
    }
}

module.exports = {general: GeneralHelpers, date: DateHelpers};